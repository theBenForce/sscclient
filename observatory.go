package sscclient

import (
	"time"
)

// Observatory retrieved from a list of available observatories
type Observatory struct {
	ID         string `xml:"Id"`
	Name       string
	Resolution uint16
	StartTime  time.Time
	EndTime    time.Time
	ResourceID string `xml:"ResourceId"`
}

type observatoryResponse struct {
	Observatories []Observatory `xml:"Observatory"`
}
