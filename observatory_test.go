package sscclient

import (
	"encoding/xml"
	"testing"

	"github.com/stretchr/testify/assert"
)

var aceData []byte = []byte("<Observatory><Id>ace</Id><Name>ACE</Name><Resolution>720</Resolution><StartTime>1997-08-25T17:48:00.000Z</StartTime><EndTime>2018-04-23T23:36:00.000Z</EndTime><Geometry></Geometry><TrajectoryGeometry></TrajectoryGeometry><ResourceId>spase://SMWG/Observatory/ACE</ResourceId></Observatory>")

func TestObservatoryParsing(t *testing.T) {
	var observatory Observatory

	xml.Unmarshal(aceData, &observatory)

	assert.EqualValues(t, "ace", observatory.ID)
	assert.EqualValues(t, "ACE", observatory.Name)
	assert.EqualValues(t, 720, observatory.Resolution)
}

func TestListObservatories(t *testing.T) {
	client := NewClient(nil)

	observatories, _ := client.ListObservatories()

	assert.NotEmpty(t, observatories)
}
