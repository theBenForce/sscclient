package sscclient

import (
	"encoding/xml"
	"time"
)

type timeInterval struct {
	Start time.Time
	End   time.Time
}

type DataRequest struct {
	XMLName      xml.Name `xml:"http://sscweb.gsfc.nasa.gov/schema"`
	TimeInterval timeInterval
}

func (r DataRequest) SetStartTime(value time.Time) DataRequest {
	r.TimeInterval.Start = value
	return r
}
