# SSC Client

SSC Client is a GO library to get current satellite data from NASA's [SSC Rest API](https://sscweb.gsfc.nasa.gov/WebServices/REST/).


## Examples

### Get a list of available satellites
```go
package main

import "gitlab.com/theBenForce/sscclient"

func main() {
    client := NewClient(nil)
    observatories, _ := client.ListObservatories()
}
```