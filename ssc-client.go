package sscclient

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"net/url"
)

// SSCClient manages requests to/from the SSC API
type SSCClient struct {
	BaseURL    *url.URL
	httpClient *http.Client
	basePath   string
}

// NewClient creates a new instance of the SSCClient
func NewClient(httpClient *http.Client) *SSCClient {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	c := &SSCClient{httpClient: httpClient}
	c.BaseURL, _ = url.Parse("https://sscweb.sci.gsfc.nasa.gov/")
	c.basePath = "WS/sscr/2"

	return c
}

func (c *SSCClient) newRequest(method, path string) (*http.Request, error) {
	rel := &url.URL{Path: c.basePath + path}
	u := c.BaseURL.ResolveReference(rel)

	req, err := http.NewRequest(method, u.String(), nil)

	if err != nil {
		return nil, err
	}

	req.Header.Set("Accept", "application/xml")

	return req, nil
}

func (c *SSCClient) execute(req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.httpClient.Do(req)

	if err != nil {
		return nil, err
	}

	// defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	err = xml.Unmarshal(data, v)
	return resp, err
}

func (c *SSCClient) ListObservatories() ([]Observatory, error) {

	req, err := c.newRequest("GET", "/observatories")

	if err != nil {
		return nil, err
	}

	var root observatoryResponse
	_, err = c.execute(req, &root)

	if err != nil {
		return nil, err
	}

	return root.Observatories, nil
}
